const http = require('http');
const querystring = require('querystring');
const external_service_host = 'netology.tomilomark.ru';
const external_service_path = '/api/v1/hash';
const content = require('./content'); 
const html_content1 = content.html_content1;
const html_content2 = content.html_content2;
const port = 3000;
let request = http.request;

function serverHandler(req, res) { 
	let url = req.url;
	let service_response='';
	console.log(`req.url= %s`,url);
	switch(url){
		case '/': {
			res.writeHead(200, 'OK', {'Content-Type': 'text/html'	}); 
			res.write(html_content1); 
			res.end();
            break; 
		}
		case '/post':{
          console.log(`post handler`);
          let data ='';
          let post;
          req.on('data', (chunk)=>{
          	data+=chunk;
          });
          req.on('end',()=>{
          	post = querystring.parse(data);
          	console.log('post.first_name %s', post['first_name']);
          	console.log('post.second_name %s',post.second_name);
            res.writeHead(200, 'OK', {'Content-Type': 'text/json'	}); 
		    //res.write(`<h1>Hello ${post['first_name']}</h1>`);
		    
		    let second_name = JSON.stringify({
		    	'lastName' : post.second_name
		    }); 
		    console.log('second_name %s', second_name);
		    let personalData = querystring.stringify({ 
		    	'First_name': post.first_name, 
		    	'second_name': post.second_name 
		    }); 
		    let options = { 
		    	hostname: external_service_host, 
		    	port: 80, 
		    	path: external_service_path, 
		    	method: 'POST', 
		    	headers: { 
		    		'Content-Type': 'application/json', 
		    		'Content-Length': Buffer.byteLength(second_name),
		    		'Firstname': post.first_name
		    	} 
		    }
		    request = http.request(options);
		    request.write(second_name);
            request.on('response', (response)=>{
            	external_service_response = '';
                response.on('data', function (chunk) {
                  external_service_response += chunk;
                });
                response.on('end', function () {
                  console.log('Response')
                  console.log("data %s", external_service_response);
                  let hash = JSON.parse(external_service_response).hash;
                  let own_service_response = {
                  	'Firstname': post.first_name,
                  	'lastName': post.second_name,
                  	'hash': hash
                  };
                  res.write(JSON.stringify(own_service_response));
                  res.end();
                });
            });

            request.end();
            //res.write(JSON.stringify(service_response));
            
          })
		  //res.write(html_content2);
          console.log("Ku-ku", req.body);
          break;
		}	
	}
	 
}

const server = http.createServer(); 
server.on('error', err => console.error(err)); 
server.on('request', serverHandler); 
server.on('listening', () => { 
	console.log('Start HTTP on port %d', port); 
}); 
server.listen(port);

